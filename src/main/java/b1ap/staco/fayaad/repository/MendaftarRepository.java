package b1ap.staco.fayaad.repository;

import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.ikhsan.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MendaftarRepository extends JpaRepository<Mendaftar, String> {
    Mendaftar findByIdmendaftar(int idmendaftar);

    List<Mendaftar> findAllByUser(User user);
}