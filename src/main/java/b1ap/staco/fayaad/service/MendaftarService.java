package b1ap.staco.fayaad.service;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.ikhsan.model.User;

public interface MendaftarService {

    Mendaftar createPendaftaran(Mendaftar mendaftar, Matkul matkul, User user);

    Mendaftar createMendaftar(Mendaftar mendaftar, Matkul matkul, User user);

    void addMendaftarToUser(Mendaftar mendaftar, User user);

    void addMendaftarToMatkul(Mendaftar mendaftar, Matkul matkul);

    Mendaftar getMendaftar(String idmendaftar);

    Mendaftar getMendaftarByUserMatkul(User user, Matkul matkul);

    Iterable<Mendaftar> getListMendaftar(Matkul matkul);

    Mendaftar setAsdos(Mendaftar mendaftar);

    Mendaftar dropAsdos(Mendaftar mendaftar);

    Mendaftar addGroup(String idmendaftar, String idpendaftar);

    Mendaftar dropGroup(String idmendaftar);

    Mendaftar setKodeAsdos(String idmendaftar, String kode_asdos);

    void deleteMendaftar(String idmendaftar);
}
