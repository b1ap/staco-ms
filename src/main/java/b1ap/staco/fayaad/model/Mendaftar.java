package b1ap.staco.fayaad.model;
import b1ap.staco.ikhsan.model.User;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "mendaftars")
@NoArgsConstructor
public class Mendaftar {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idmendaftar", updatable = false, nullable = false)
    private int idmendaftar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID", nullable = false)
    private Matkul matkul;

    @Column(name = "kode_asdos")
    private String kode_asdos;

    @Column(name = "isAsdos")
    private Boolean isAsdos;

    public Mendaftar(User user, Matkul matkul, String kode_asdos, boolean isAsdos){
        this.user = user;
        this.matkul = matkul;
        this.kode_asdos = kode_asdos;
        this.isAsdos = isAsdos;
    }

    public int getIdmendaftar() {
        return idmendaftar;
    }

    public Matkul getMatkul() {
        return matkul;
    }

    public User getUser() {
        return user;
    }

    public Boolean getIsAsdos() {
        return isAsdos;
    }

    public String getKode_asdos() {
        return kode_asdos;
    }

    public void setAsdos(boolean asdos) {
        isAsdos = asdos;
    }

    public void setMatkul(Matkul matkul) {
        this.matkul = matkul;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setIdmendaftar(int idmendaftar){
        this.idmendaftar = idmendaftar;
    }

    public void setKode_asdos(String kode_asdos) {
        this.kode_asdos = kode_asdos;
    }
}