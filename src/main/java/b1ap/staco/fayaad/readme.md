[![pipeline report](https://gitlab.com/b1ap/staco/badges/fayaad/pipeline.svg)](https://gitlab.com/b1ap/staco/-/commits/fayaad)
[![coverage report](https://gitlab.com/b1ap/staco/badges/fayaad/coverage.svg)](https://gitlab.com/b1ap/staco/-/commits/fayaad)


# Matkul
## Requirements

### SQL Table

USER \
<u>Username</u>, Email, password, nama_depan, nama_belakang, isAdmin, NPM

MENDAFTAR \
<u>idmendaftar</u>, Username, IDmatkul, Kode_asdos, isAsdos

MATKUL\
<u>ID</u>, Nama_matkul, Kode_matkul, username_pembuat


### Urls
Login As Admin : 
- Melihat Matkul (GET)
   ```
    /matkul/dev
   ```
- Membuat Matkul Form(GET)
   ```
    /matkul/dev/create
   ```
- Membuat Matkul (POST)
   ```
  /matkul/dev/create
   ```
- Melihat Informasi Matkul (GET)
   ```
    /matkul/dev/[IDmatkul]
   ```
- Menjadikan sebagai Asdos (GET)
  ```
    /matkul/dev/set/[idmendaftar]
  ```
- Menjadikan sebagai Mahasiswa (GET)
  ```
    /matkul/dev/drop/[idmendaftar]
  ```
SOON:
- Mengedit Matkul (POST)
   ```
    /matkul/dev/[IDmatkul]
   ```
- Menghapus Matkul (GET)
   ```
    /matkul/dev/[IDmatkul]/remove
   ```

Login As Non-Admin :
- Melihat Matkul (GET)
   ```
    /matkul
   ```
- Mendaftar Matkul FORM (GET)
   ```
    /matkul/join
   ```
- Mendaftar Matkul (POST)
   ```
    /matkul/join
   ```
- Melihat Informasi Matkul (GET)
   ```
    /matkul/[IDmatkul]
   ```
- Menambahkan Anggota Asdos (GET)
   ```
    /matkul/[IDmatkul]/add/[idmendaftarmhs]/[idmendaftarasdos]
   ```
- Mengeluarkan Anggota Asdos (GET)
   ```
    /matkul/[IDmatkul]/drop/[idmendaftarmhs]/[idmendaftarasdos]
   ```
- Mengatur Kode Asdos (GET)
   ```
    /matkul/[IDmatkul]
   ```
  
SOON:
- keluar Matkul (GET)
   ```
    /matkul/[IDmatkul]/leave
   ```