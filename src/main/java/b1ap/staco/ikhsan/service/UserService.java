package b1ap.staco.ikhsan.service;

import b1ap.staco.ikhsan.model.User;

public interface UserService {

    User getUserByUsername(String username);

    User addNewUser(User user);

}
