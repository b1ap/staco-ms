package b1ap.staco.fayaad.model;

import b1ap.staco.ikhsan.model.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MendaftarTest {

    @Test
    public void testSetterAndGetterMendaftar() {
        Matkul matkul1 = new Matkul("Adpro", "CSGE123");
        Matkul matkul2 = new Matkul("Alin", "CSCM123");
        User user1 = new User("tes", "tes", "tes",  "tes", "tes@test", false, "123");
        Mendaftar mendaftar1 = new Mendaftar();
        mendaftar1.setIdmendaftar(10);
        mendaftar1.setMatkul(matkul1);
        mendaftar1.setUser(user1);
        mendaftar1.setKode_asdos("YET");
        mendaftar1.setAsdos(true);

        Mendaftar mendaftar2 = new Mendaftar(user1, matkul2, "YET", true);

        assertThat(mendaftar1.getIdmendaftar()).isNotNull();
        assertThat(mendaftar1.getUser()).isNotNull();
        assertThat(mendaftar1.getMatkul()).isNotNull();
        assertThat(mendaftar1.getKode_asdos()).isNotNull();
        assertThat(mendaftar1.getIsAsdos()).isNotNull();
        assertThat(mendaftar2.getIdmendaftar()).isNotNull();
        assertThat(mendaftar2.getUser()).isNotNull();
        assertThat(mendaftar2.getMatkul()).isNotNull();
        assertThat(mendaftar2.getKode_asdos()).isNotNull();
        assertThat(mendaftar2.getIsAsdos()).isNotNull();
    }

}