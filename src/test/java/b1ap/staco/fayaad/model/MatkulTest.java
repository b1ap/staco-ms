package b1ap.staco.fayaad.model;

import b1ap.staco.ikhsan.model.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MatkulTest {

    @Test
    public void testSetterAndGetterMatkul() {
        Matkul matkul1 = new Matkul();

        matkul1.setID(10);
        matkul1.setNama_matkul("Alin");
        matkul1.setKode_matkul("CSCM123");

        Matkul matkul2 = new Matkul("Adpro", "CSGE123");

        Mendaftar mendaftar1 = new Mendaftar();
        List<Mendaftar> mendaftarList = Arrays.asList(mendaftar1);

        matkul1.getMatkulDidaftar().add(mendaftar1);

        assertThat(matkul1.getID()).isNotNull();
        assertThat(matkul1.getNama_matkul()).isNotNull();
        assertThat(matkul1.getKode_matkul()).isNotNull();
        assertThat(matkul2.getID()).isNotNull();
        assertThat(matkul2.getNama_matkul()).isNotNull();
        assertThat(matkul2.getKode_matkul()).isNotNull();
        assertThat(matkul2.getKode_matkul()).isNotNull();
        assertEquals(matkul1.getMatkulDidaftar(), mendaftarList);

    }

}