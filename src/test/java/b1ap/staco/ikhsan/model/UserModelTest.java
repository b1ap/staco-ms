package b1ap.staco.ikhsan.model;

import b1ap.staco.fayaad.model.Mendaftar;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserModelTest {

    @Test
    public void testSetterAndGetterUser() {
        User user1 = new User();

        User user2 = new User("tes", "tes", "tes",
                "tes", "tes@test", false, "123");

        user1.setUsername("test");
        user1.setPassword("test");
        user1.setfName("test1");
        user1.setlName("test2");
        user1.setEmail("test@gmail.com");
        user1.setIsAdmin(false);
        user1.setNPM("123456789");

        Mendaftar mendaftar = new Mendaftar();

        mendaftar.setUser(user1);

        assertThat(user2.getUsername()).isNotNull();
        assertThat(user2.getPassword()).isNotNull();
        assertThat(user2.getNPM()).isNotNull();
        assertThat(user1.getfName()).isNotNull();
        assertThat(user1.getlName()).isNotNull();
        assertThat(user1.getIsAdmin()).isNotNull();
        assertThat(user1.getEmail()).isNotNull();
        assertThat(user1.getMatkulDidaftar()).isNotNull();
    }

}
