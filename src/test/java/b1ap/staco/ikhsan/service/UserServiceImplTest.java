package b1ap.staco.ikhsan.service;

import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userServiceImpl;

    @MockBean
    private User testUser;

    @BeforeEach
    public void setUp() {
        testUser = new User("test", "test", "test",
                "test", "test@test", false, "123");
    }

    @Test
    public void testAddNewUser() {
        userRepository.save(testUser);

        User userTestRes = userServiceImpl.addNewUser(testUser);

        assertThat(userTestRes).isNotNull();
    }

    @Test
    public void testGetUserByUsername() {
        userRepository.save(testUser);

        lenient().when(userRepository.findByUsername("test")).thenReturn(testUser);

        User userTestRes = userServiceImpl.getUserByUsername("test");

        assertThat(userTestRes).isNotNull();
    }
}
