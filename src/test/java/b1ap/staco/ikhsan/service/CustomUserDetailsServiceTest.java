package b1ap.staco.ikhsan.service;

import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class CustomUserDetailsServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private CustomUserDetailsService customUserDetailsService;

    @MockBean
    private User testUser;

    @BeforeEach
    public void setUp() {
        testUser = new User("test", "test", "test",
                "test", "test@test", false, "123");
        userRepository.save(testUser);
    }

    @Test
    public void testCustomUserDetailsService() {
        lenient().when(userRepository.findByUsername("test")).thenReturn(testUser);
        UserDetails userDetails = customUserDetailsService.loadUserByUsername("test");

        assertThat(userDetails).isNotNull();
    }

}
